#!/bin/bash

#如果需要禁止非root身份启动服务器，则打开下面的注释
#if (( $EUID != 0 )); then
#    echo "请以root身份运行！"
#    exit 1
#fi

start () {
    nohup node app.js >log/app.log 2>&1 </dev/null &
    echo $! > log/app.pid
    echo '服务顺利启动。'
}

stop () {
    kill `cat log/app.pid`
    rm log/app.pid
    echo '服务顺利关闭。'
}

case $1 in
start)
    start
    ;;
stop)
    stop
    ;;
restart)
    stop
    start
    ;;
*)
    echo "Usage: bin/server (start|stop|restart)"
    ;;
esac

