
function Normalize (params, normalizerName) {
  const normalize = load(normalizerName)
  try {
    return normalize(params)
  } catch (err) {
    throw new Normalize.NormalizeError(err)
  }
}

function load (normalizerName) {
  try {
    const path = '../plugins/normalizers/' + normalizerName
    return require(path)
  } catch (err) {
    if (err.code === 'MODULE_NOT_FOUND') {
      throw new Normalize.LoadedError(normalizerName)
    } else {
      throw err
    }
  }
}

Normalize.LoadedError = class LoadedError extends Error {
  constructor (normalizerName) {
    super(`加载normalizer插件失败：${normalizerName}.`)
  }
}

Normalize.NormalizeError = class NormalizeError extends Error {
  constructor (err) {
    super('执行normalize操作出现未知异常。')
    this.reason = err
  }
}

module.exports = Normalize
