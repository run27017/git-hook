module.exports = {
  success: 200,
  illegal_json_format: 400,
  normalizer_not_found: 404,
  normalize_error: 500,
  config_error: 500,
  config_not_found: 404,
  handler_not_matched: 200,
  token_not_matched: 401,
  executor_not_found: 500,
  executor_loading_error: 500,
  execute_error: 500
}
