const mm = require('micromatch')
const pry = require('pryjs')

class WildcardMatcher {
  constructor (source) {
    this._source = source
  }

  match (target) {
    if (target) {
      return mm.isMatch(target, this._source)
    } else {
      return false
    }
  }
}

module.exports = WildcardMatcher

