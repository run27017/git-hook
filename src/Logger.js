/* Levels:
 *   - silly
 *   - debug
 *   - verbose
 *   - info
 *   - warn
 *   - error
 *
 */
const winston = require('winston')

winston.level = 'verbose'

module.exports = winston
