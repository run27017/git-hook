const program = require('commander')
program
  .version('0.1.0')
  .option('-p, --port [port]', 'port')
  .parse(process.argv);

const Config = {
  port: program.port || 3000
}

module.exports = Config
