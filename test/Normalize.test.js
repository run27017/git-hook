const test = require('ava')
const Normalize = require('../src/Normalize')

test('normalize successfully', t => {
  t.notThrows(() => {
    const data = Normalize({ data: '"a"' }, 'simple')
    t.is(data, 'a')
  })
})

test('normalizer required error', t => {
  t.throws(() => {
    Normalize({ data: '"a"' }, 'simple2')
  }, Normalize.LoadedError)
})

test('normalze error', t => {
  t.throws(() => {
    Normalize({ data: '"a"' }, 'error')
  }, Normalize.NormalizeError)
})
