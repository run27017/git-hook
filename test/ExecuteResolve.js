const { uid, gid, ExecuteResolveError } = require('../src/ExecuteResolve')

function capture (source) {
  try {
    return eval(source)
  } catch (err) {
    return err
  }
}

console.log('uid(1000) =>', uid(1000))
console.log('uid(hello) =>', uid('hello'))
console.log('uid(9999) =>', capture('uid(9999)'))
console.log('uid(world) =>', capture('uid("world")'))
