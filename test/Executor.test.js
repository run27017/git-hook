const test = require('ava')
const td = require('testdouble')
const Executor = require('../src/Executor')
const ExecuteResolver = require('../src/ExecuteResolver')

ExecuteResolver.uid = td.function()
td.when(ExecuteResolver.uid(td.matchers.anything())).thenReturn(1000)
ExecuteResolver.gid = td.function()
td.when(ExecuteResolver.gid(td.matchers.anything())).thenReturn(100)

const resolver = {
  uid: function () { return 1000 },
  gid: function () { return 100 }
}

test('execute touch file', t => {
  t.notThrows(() => {
    const executor = new Executor({ command: 'do nothing', executor: 'doNothing' }, resolver)
    executor.execute()
  })
})

test('executor require error', t => {
  t.throws(() => {
    new Executor({ command: 'do nothing', executor: 'doNothing2' }, resolver)
  }, Executor.LoadedError)
})
