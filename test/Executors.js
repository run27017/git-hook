const execute = require('../plugins/executors/spawn')

execute({
  command: 'ls -l && byebye',
  stdout: '/home/hello/tmp/stdout',
  stderr: '/home/hello/tmp/stdout',
  cwd: '/home/hello/tmp',
  uid: 1000,
  gid: 100
})
