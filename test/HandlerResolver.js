const util = require('util')
const HandlerResolver = require('../src/HandlerResolver')
const HandlerBuilder = function (...configs) {
  configs = configs.filter(config => config)
  return Object.assign({}, ...configs)
}

const handlerResolver = new HandlerResolver(HandlerBuilder)
console.log(util.inspect(handlerResolver._store, { showHidden: false, depth: null }))
const a = handlerResolver.resolve('/a')
console.log('matched', a.match())
console.log('handled', a.handle())
//console.log(handlerResolver.resolve('b', {}))
