const test = require('ava')
const Handler = require('../src/Handler')
const WildcardMatcher = require('../src/WildcardMatcher')
const Executor = require('../src/Executor')

const resolver = {
  uid: function () { return 1000 },
  gid: function () { return 100 }
}

let handler = null

test.beforeEach(t => {
  handler = new Handler({
    token: 'mingmingpao',
    eventMatcher: new WildcardMatcher('push'),
    branchMatcher: new WildcardMatcher('master'),
    executor: new Executor({ command: 'do nothing', executor: 'doNothing' }, resolver)
  })
})

test('success', t => {
  let result = handler.handle({ token: 'mingmingpao', branch: 'master', event: 'push' })
  t.is(result.code, 'success')
})

test('token should matched', t => {
  let result = handler.handle({ token: 'mingmingpao1', branch: 'master', event: 'push' })
  t.is(result.code, 'token_not_matched')
  result = handler.handle({ branch: 'master', event: 'push' })
  t.is(result.code, 'token_not_matched')
})

test('matches', t => {
  t.true(handler.match({ branch: 'master', event: 'push', }))
  t.false(handler.match({ branch: 'master2', event: 'push', }))
  t.false(handler.match({ branch: 'master', event: 'push2', }))
})
