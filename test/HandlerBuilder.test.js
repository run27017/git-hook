const test = require('ava')
const Handler = require('../src/Handler')
const HandlerBuilder = require('../src/HandlerBuilder')

test('success', t => {
  const handler = HandlerBuilder({
    command: 'do nothing',
    executor: 'doNothing',
    cwd: '',
    user: 100,
    group: 1000
  })
  t.true(handler instanceof Handler)
})

// 这一块的测试我已经忘记是怎么回事了
test('missing executor', t => {
  const handler = HandlerBuilder({})
  const { code, message } = handler.handle()
  t.is(code, 'config_error')
  t.log(message)
})

test('build config', t => {
  const config = HandlerBuilder._buildConfig([{'a': 'a'}, undefined, {'a': 'a2', 'b': 'b'}])
  t.deepEqual(config, {'a': 'a2', b: 'b'})
})
