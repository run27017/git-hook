const test = require('ava')
const WildcardMatcher = require('../src/WildcardMatcher')

test('wildcard match', t => {
  const matcher = new WildcardMatcher('foo.*')
  t.true(matcher.match('foo.bar'))
})

test('should not match null', t => {
  const matcher = new WildcardMatcher('foo.*')
  t.false(matcher.match(undefined))
})

