/* 该executor仅仅是同步打印传入的参数，可以作为调试。 */

module.exports = function ({ command, cwd, stdout, stderr, uid, gid }) {
  console.log('debug execute', { command, cwd, stdout, stderr, uid, gid })
}
