# git-hook

git-hook是一个搭建在远端服务器上的git钩子服务，它有着远大的理想，但目前实现较为简单。现在绝大多数git仓库管理平台都支持设置一个回调URL，包括github、oschina、Coding等。当开发者在本地push代码到远程时，就会向该URL发起请求，接收该请求的服务器可以在本地运行一个配置好的命令。该工具就是用来管理这些命令的。

特性：

- 单服务器，需要在运行命令的服务器上搭建本服务。
- 支持多平台，包括github、oschina、coding等。
- YAML配置文件，语法好用简洁。
- 命令的配置支持user、group、cwd、stdout和stedrr的重定向。
- 友好的错误提示以及日志。

## 快速部署

前置条件：

1. Linux系统
2. 已安装node和yarn，node版本需要在v6及以上

部署步骤：

1. `yarn`
2. 参考配置文件规则部分
3. 启动：`yarn start`

重启：

> 在任何配置文件修改的时候，都需要重启服务以重载配置。

### 关于sudo

服务的启动一定要加入sudo来以root身份运行。因为工具支持切换用户和用户组执行命令，只有root用户才能无缝支持用户和用户组的切换，其他身份切换都需要提供密码。当然，也可以不以root身份运行，但在配置文件内的user和group字段必须设置为当前用户和组，否则工具就会报错。

### 命令参数

服务器启动时支持一些参数：

- -p/--port {port}: 端口

## 配置文件规则

hook配置存放在config目录下，配置文件的结构参考examples/simple-hook.yml和examples/full-hook.yml文件。配置文件需要以.yml结尾，其他的任何后缀都会略过。

待配置的URL与配置文件的路径是一一对应的关系，例如相对于config目录的配置文件a.yml，它的URL子路径必须是/a，相对于config目录的配置文件a/b.yml，他的URL子路径必须是/a/b.yml.

配置文件的字段解释如下：

- `token`: 一个密钥，用于验证请求的有效性。当设置此字段时，任何token不匹配的请求都会被拒绝；当没有设置此字段时，不作验证，接受任何请求。强烈建议设置此字段。
- `branch`: 一个通配符形式的分支字段过滤器。任何不匹配的请求都会被当前配置拒绝，但一个配置文件内有多个配置，后续的配置仍然有可能接受此请求。默认值为`*`.
- `event`：一个通配符形式的事件字段过滤器，事件包括push、merge\_request等。任何不匹配的请求都会被当前配置拒绝，但一个配置文件内有多个配置，后续的配置仍然有可能接受此请求。默认值为`*`.
- `executor`：配置执行器，具体参见命令与环境一节。对于一般用户来说，该字段填spawn就够了。
- `command`：执行的命令。
- `cwd`：命令执行环境的当前目录。
- `user`：命令执行的用户。
- `group`：命令执行的用户组。
- `stdout`：stdout重定向的文件路径。
- `stderr`：stderr重定向的文件路径。

基本上所有字段都是必填的，最好都写上去。

### 特殊的配置文件default.yml和特殊的配置键default

根目录的default.yml内配置的内容会作为其他所有配置的默认值，参考examples/default.yml.

配置文件中键default对应的配置作为该文件中其他配置的默认值，参考examples/full-hook.yml.

综上，字段的覆盖规则为：特殊键、default键、default.yml.

## Hook的URL规则

一个完整的URL格式为：

    http://{domain}:{port}/{path}?normalizer={normalizer}

该URL每个部分的解释为：

- `domain`: 能够访问到服务器的域名。
- `port`: 服务器启动的端口，该端口可以在启动时通过参数指定，默认为3000.
- `path`: path与配置文件的位置是一一对应的。
- `normalizer`: normalizer指定了规范化的方法，它一般是平台的名字，如github、oschina、coding等。目前只支持Coding，后续逐步加入其他的支持。也可以自己编写normalizer插件。

例如，你在coding.net平台上设置hook，并且配置文件在config/a/b.yml下，则路径地址一定得配置为

    http://your-domain:port/a/b?normalizer=coding

## 命令与环境

Linux的命令与执行环境有很大的关系，一时间很难彻底理清。Node提供了一个调用外部命令的方法`child_process.spawn`，但这个方式在执行某些命令时会产生一些说不清楚的问题。为了解决这一个问题，git-hook工具将命令的执行权完全交个用户自定义。我们不定死命令执行的方式，而是在plugins/executors内动态指定的。

配置文件内有个executor字段，它指定执行命令的执行器，该执行器是plugins/executors目录下的js文件。例如该目录下有一个spawn.js文件，则executor设为spawn会指定该文件作为执行器。

## 如何自己写插件

插件分为执行器插件和规范器插件，都放置在plugins的对应目录中。插件都是一个js文件，该文件表示一个node模块。

### Executor插件

执行器插件是真实运行命令的，一个执行器插件是一个js文件，它表示一个node模块，该模块返回一个函数。执行器插件放置在plugins/executors/目录下。一个执行器模块的典型形式是：

```javascript
module.exports = function ({ command, cwd, uid, gid, stderr, stdout }) {
  // 执行command并写入到stdout、stderr的代码
}
```

### Normalizer插件

规范器插件将原始的json请求体转化为工具识别的请求体。通过规范器插件，我们可以达到支持github、oschina、coding等好多不同代码托管平台的效果。

一个规范器插件是一个js文件，它表示一个node模块，该模块返回一个函数。规范器插件放置在plugins/normalizers目录下，它的典型形式是：

```
module.exports = function ({ path, query, data, headers, request }) {
  return normalizedData
}
```

## 开发与调试

首先，复制 `examples/simple-hook.yml` 到 `config/simple-hook.yml`，认真修改每一项。

然后，启动开发服务器：

```bash
yarn dev
```

在命令行下调用如下请求试一试：

```bash
curl -XPOST http://localhost:3000/simple-hook -d '{"branch":"master", "event":"test", "token":"token"}'
```


## 未竟之志

- [ ] 日志文件创建文件夹失败
- [ ] 在HandlerResolver.js下面console.log会导致服务器启动问题
- [ ] coding normalizer JSON错误
